# Reto-DevOps CLM

## El reto comienza aquí
Se crea un Fork, para que genere la copia de reto publico de CLM.

Para trabajar en el siguiente reto se generaron ramas por cada reto realizado. De igual manera todo quedo en la master realizando el merge request.

### Reto 1. Dockerize la aplicación

Se crea un dockerfile con las instrucciones que quiero que ejecute mi imagen esta partiendo desde un imagen de node:15-alpine.  
Se crea el archivo .dockerignore, para que docker build excluya los archivos mencionados a la hora de ejecutar la imagen. 
Se coloco en el docker file unas instrucciones para que genere el usuario y luego se luego se le da permiso a ese usuario con chown y de esta manera pueda ejecutarse con un usuario diferente de root.


### Reto 2. Docker Compose

1. Nginx que funcione como proxy reverso a nuesta app Nodejs
Se genera el archivo docker-compose.yml donde se encuentra la aplicacion NGINX, y se envia como volumen los archivos contruidos, a la imagen que se esta generando, se contruyen los certificados.key y certificado.crt aplicando el comando 
//openssl req -x509 -nodes -sha256 -newkey rsa:2048 -keyout <<<<<<key.key>>>>>>> -out <<<<<<crt.crt>>>>>>> -days 3650//.
Se genera un usuario y clave a traves de un contenedor de debian para poder asegurar el /private, . De igual manera se configura el archivo nginx.conf, donde se especifica el proxy pass para pegarle a la aplicacion de nginx, al igual que se redirecciona el trafico mediante esta configuracion.  

### Reto 3. Probar la aplicación en cualquier sistema CI/CD

Se realizar un pipeline que cuanta solo con tres job: build, test y package. El job de build ejecuta el npm installa y el de test la ejecucion de el run test, estos dos trabajan con una misma imagen de node:15-alpine. El job de package cambia la imagen a docker y realizamos un docker in docker, en el cual se sube la imagen al registry personal de docker hub. Imagen del registry: yramirezclm/reto-devops:2.0.0. No se realizo el job de deployment porque tengo entendido que depende de lo ejecutado en helm para poder armarlo. 

### Reto 4. Deploy en kubernetes

En esta parte se generaron los manifiestos para poder desplegar la aplicacion e kubernetes, los cuales se encuentran en el directorio de k8s: deployment (con el fin de porporcionar actualizaciones para los pod y los replicasets), service (se crea para que se balancee la carga y poder conectarme dentro de esa imagen),hpa (este para variar el número de pods desplegados mediante un replication controller), estos archivos se le proporcionan la informacion a kubectl como archivo ymal. En la ejecucion se crearon namespace para dividir la aplicacion de forma mas ordenada. 

### Reto 5. Construir chart en helm y manejar trafico http(s)
Se levanto el chart en base a los manifiestos de kubernetes.



