FROM node:15-alpine

RUN addgroup -S user-node 
RUN adduser -S user-node -G user-node 

RUN mkdir /opt/app

COPY . /opt/app
RUN chown -R user-node.user-node  /opt/app
USER user-node
WORKDIR /opt/app
RUN npm install
RUN npm run test
CMD ["node","index.js"]